import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { AuthData } from './auth-data.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private isAuthenticated= false;
  private token: string;
  private tokenTimer : any;
  private authStatusListener = new Subject<boolean>();

  constructor(private http: HttpClient, private router: Router) { }
  
  getToken(){
    return this.token;
  }

  getIsAuth(){
    return this.isAuthenticated;
  }

  getauthStatusListener(){
    return this.authStatusListener.asObservable();
  }

  createUser(email:string, password: string){
    const authdata: AuthData = {email: email, password: password}
    this.http.post('http://localhost:3001/api/user/signup', authdata)
      .subscribe(()=>{
        this.router.navigate(['/'])
      }, error =>{
        console.log(error);
        
      })

  }
  login(email: string, password: string){
    const authdata: AuthData = {email: email, password: password}
    this.http.post<{token: string, expiresIn: number}>('http://localhost:3001/api/user/login', authdata)
    .subscribe(response =>{
      const token = response.token;
      this.token = token;
      if(token){
        const expiresInDuration = response.expiresIn;
        this.tokenTimer = setTimeout(() => {
          this.logout();
        }, expiresInDuration * 1000);
        this.isAuthenticated= true;
        this.authStatusListener.next(true);
        const now = new Date();
        const expirationDate = new Date(now.getTime() + expiresInDuration * 1000);
        console.log(expirationDate);
        this.saveAuthData(token , expirationDate)
        this.router.navigate(['/']);
      }
    })
  }
  logout(){
    this.token = null;
    this.isAuthenticated = false;
    this.authStatusListener.next(false);
    clearTimeout(this.tokenTimer);
    this.clearAuthData();
    this.router.navigate(['/']);
  }
  private saveAuthData(token: string, expirationDate: Date){
    localStorage.setItem("token", token);
    localStorage.setItem("expiration", expirationDate.toISOString());
  }
  private clearAuthData(){
    localStorage.removeItem("token"),
    localStorage.removeItem("expiration");
  }

}
