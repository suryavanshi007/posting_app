import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  isLoading = false;
  private authStatSub = Subscription;
  constructor(private authservice: AuthService) { }
  onSignup(form: NgForm){
    if(form.invalid){
      return 
    }else{
      this.authservice.createUser(form.value.email, form.value.password);
    }
  }
  ngOnInit(): void {
    // this.authStatSub = this.authservice.getauthStatusListener().subscribe({
    //   authstatus =>{

    //   }
    // });
  }

}
